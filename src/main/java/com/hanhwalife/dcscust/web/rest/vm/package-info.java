/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hanhwalife.dcscust.web.rest.vm;
