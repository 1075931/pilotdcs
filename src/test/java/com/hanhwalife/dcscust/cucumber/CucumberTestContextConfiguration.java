package com.hanhwalife.dcscust.cucumber;

import com.hanhwalife.dcscust.CustomerApp;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = CustomerApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
