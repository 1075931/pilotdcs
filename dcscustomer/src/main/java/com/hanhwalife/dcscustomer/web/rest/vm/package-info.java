/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hanhwalife.dcscustomer.web.rest.vm;
